package nl.vandervlist.newsscraper.service

import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import nl.vandervlist.newsscraper.config.BlockConfig
import nl.vandervlist.newsscraper.config.FeedItemConfig
import nl.vandervlist.newsscraper.config.FeedScraperConfig
import nl.vandervlist.newsscraper.model.Post
import nl.vandervlist.newsscraper.repository.PostsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.net.URL
import java.time.ZoneId

@Service
class FeedScraperService @Autowired constructor(
        blockConfig: BlockConfig,
        postsRepository: PostsRepository,
        val feedScraperConfig: FeedScraperConfig
) : Scraper(
        blockConfig,
        postsRepository
) {

    override fun loadPosts() = feedScraperConfig.feeds.flatMap { getFeed(it) }

    fun getFeed(feedConfig: FeedItemConfig): List<Post> {
        val input = SyndFeedInput().apply { isPreserveWireFeed = true }
        val feed = input.build(XmlReader(URL(feedConfig.url)))

        return feed.entries.map {
            Post(
                    it.link,
                    it.title.take(250),
                    it.publishedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                    "Feed",
                    feed.title,
                    feedConfig.category
            )
        }.filter(feedConfig.include, feedConfig.exclude)
    }
}
