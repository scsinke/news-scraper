package nl.vandervlist.newsscraper.service

import nl.vandervlist.newsscraper.config.BlockConfig
import nl.vandervlist.newsscraper.config.HackerNewsScraperConfig
import nl.vandervlist.newsscraper.model.HackerNewsItem
import nl.vandervlist.newsscraper.model.Post
import nl.vandervlist.newsscraper.repository.PostsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.sql.Timestamp
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

@Service
class HackerNewsScraperService @Autowired constructor(
        blockConfig: BlockConfig,
        postsRepository: PostsRepository,
        restTempleteBuilder: RestTemplateBuilder,
        val hackerNewsScraperConfig: HackerNewsScraperConfig
) : Scraper(blockConfig, postsRepository) {
    val restTemplate: RestTemplate = restTempleteBuilder.build()

    override fun loadPosts() = restTemplate.getForObject(
            "https://hacker-news.firebaseio.com/v0/topstories.json",
            Array<Int>::class.java
    )
            ?.mapNotNull {
                restTemplate.getForObject(
                        "https://hacker-news.firebaseio.com/v0/item/$it.json",
                        HackerNewsItem::class.java
                )
            }
            ?.filter { it.url != null && it.time != null && it.score != null }
            ?.filter {
                Duration.between(Timestamp(it.time!! * 1000)
                        .toLocalDateTime()
                        .atZone(ZoneId.systemDefault())
                        .toInstant(), Instant.now()).toMinutes() <= 60 * hackerNewsScraperConfig.maxAge &&
                        it.score!! > hackerNewsScraperConfig.minScore
            }
            ?.map {
                Post(
                        it.url!!,
                        it.title.take(250),
                        LocalDateTime.now(),
                        "Hacker News",
                        null,
                        hackerNewsScraperConfig.category
                )
            }
            ?.filter(hackerNewsScraperConfig.include, hackerNewsScraperConfig.exclude)
            ?: emptyList()
}
