package nl.vandervlist.newsscraper.service

import nl.vandervlist.newsscraper.model.Post

fun List<Post>.filter(include: List<String>, exclude: List<String>): List<Post> = this.filter { post ->
    if (include.isEmpty()) return@filter true

    include.forEach {
        if (post.title.toLowerCase().contains(it.toLowerCase())) return@filter true
    }
    return@filter false
}.filter { post ->
    if (exclude.isEmpty()) return@filter true

    exclude.forEach {
        if (post.title.toLowerCase().contains(it.toLowerCase())) return@filter false
    }
    return@filter true
}
