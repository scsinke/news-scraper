package nl.vandervlist.newsscraper.config

import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FlywayConfig {

    @Value("\${DATABASE_HOST:localhost}")
    var host: String? = null

    @Value("\${DATABASE_PORT:5432}")
    var port: String? = null

    @Value("\${DATABASE_NAME:news-scraper}")
    var database: String? = null

    @Value("\${DATABASE_USERNAME:news-scraper}")
    var username: String? = null

    @Value("\${DATABASE_PASSWORD:news-scraper}")
    var password: String? = null

    @Bean
    fun flyway(): Flyway {
        val flyway = Flyway.configure()
                .dataSource("jdbc:postgresql://$host:$port/$database", username, password)
                .load()
        flyway.migrate()

        return flyway
    }
}
