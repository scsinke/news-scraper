package nl.vandervlist.newsscraper.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties("scraper.reddit")
class RedditScraperConfig {
    var subreddits: List<SubRedditConfig> = emptyList()
}

class SubRedditConfig {
    var name = ""
    var category = ""
    var maxAge = 0
    var minScore = 0
    var flair = FilterItems()
}

class FilterItems {
    var include = emptyList<String>()
    var exclude = emptyList<String>()
}
