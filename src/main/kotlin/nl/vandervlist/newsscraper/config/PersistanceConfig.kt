package nl.vandervlist.newsscraper.config

import org.apache.ibatis.session.SqlSessionFactory
import org.mybatis.spring.SqlSessionFactoryBean
import org.mybatis.spring.annotation.MapperScan
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
@MapperScan("nl.vandervlist.newsscraper")
class PersistanceConfig {

    @Value("\${DATABASE_HOST:localhost}")
    var host: String? = null

    @Value("\${DATABASE_PORT:5432}")
    var port: String? = null

    @Value("\${DATABASE_NAME:news-scraper}")
    var database: String? = null

    @Value("\${DATABASE_USERNAME:news-scraper}")
    var username: String? = null

    @Value("\${DATABASE_PASSWORD:news-scraper}")
    var password: String? = null

    @Bean
    fun dataSource(): DataSource =
            DataSourceBuilder.create()
                    .url("jdbc:postgresql://$host:$port/$database")
                    .username(username)
                    .password(password)
                    .build()

    @Bean
    fun sqlSessionFactory(): SqlSessionFactory? {
        val factoryBean = SqlSessionFactoryBean()
        factoryBean.setDataSource(dataSource())
        return factoryBean.getObject()
    }
}
