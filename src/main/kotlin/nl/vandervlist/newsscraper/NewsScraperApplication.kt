package nl.vandervlist.newsscraper

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class NewsScraperApplication

fun main(args: Array<String>) {
    runApplication<NewsScraperApplication>(*args)
}
