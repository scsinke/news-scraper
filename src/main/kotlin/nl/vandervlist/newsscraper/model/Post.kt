package nl.vandervlist.newsscraper.model

import java.time.LocalDateTime

data class Post(
        val url: String,
        val title: String,
        val additionDate: LocalDateTime,
        val source: String,
        val subSource: String?,
        val category: String
)
