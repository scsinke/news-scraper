package nl.vandervlist.newsscraper.model

data class HackerNewsItem(
        val id: Int,
        val time: Long?,
        val url: String?,
        val by: String?,
        val descendants: Int?,
        val kids: List<Int>?,
        val score: Int?,
        val title: String,
        val type: String?
)
