package nl.vandervlist.newsscraper.model

import com.fasterxml.jackson.annotation.JsonProperty

class RedditResponse {
    val kind: String = ""
    val data: RedditResponseData = RedditResponseData()
}

class RedditResponseData {
    val children: List<RedditItem> = emptyList()
}

class RedditItem {
    var kind: String = ""
    var data: RedditItemData = RedditItemData()
}

class RedditItemData {
    var title: String = ""
    var subreddit_name_prefixed: String = ""
    var url: String = ""
    var created: Long = 0
    var ups: Int = 0
    var downs: Int = 0
    var author_flair_text: String? = ""
    @JsonProperty
    var is_self: Boolean = false
    @JsonProperty
    var is_reddit_media_domain: Boolean = false
}
