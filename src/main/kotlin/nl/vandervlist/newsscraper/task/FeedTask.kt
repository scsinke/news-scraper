package nl.vandervlist.newsscraper.task

import nl.vandervlist.newsscraper.service.FeedScraperService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class FeedTask @Autowired constructor(
        val feedScraperService: FeedScraperService
) {

    val logger: Logger = LoggerFactory.getLogger(FeedTask::class.java)

    @Scheduled(fixedDelay = 900_000)
    fun getFeeds() {
        logger.info("Getting posts from feeds")
        val size = feedScraperService.getPosts()
        logger.info("Added $size posts from feeds")
    }
}
