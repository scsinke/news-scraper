package nl.vandervlist.newsscraper.task

import nl.vandervlist.newsscraper.service.RedditScraperService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class RedditTask @Autowired constructor(
        val scraperService: RedditScraperService
) {

    val logger: Logger = LoggerFactory.getLogger(RedditTask::class.java)

    @Scheduled(fixedDelay = 900_000)
    fun getReddit() {
        logger.info("Getting posts from Reddit")
        val size = scraperService.getPosts()
        logger.info("Added $size posts from Reddit")
    }
}
