package nl.vandervlist.newsscraper.task

import nl.vandervlist.newsscraper.service.HackerNewsScraperService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class HackerNewsTask @Autowired constructor(
        val scraperService: HackerNewsScraperService
) {

    val logger: Logger = LoggerFactory.getLogger(HackerNewsTask::class.java)

    @Scheduled(fixedDelay = 900_000)
    fun getHackerNews() {
        logger.info("Getting posts from Hacker News")
        val size = scraperService.getPosts()
        logger.info("Added $size posts from Hacker News")
    }
}
